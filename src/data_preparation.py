import numpy as np
import pandas as pd
from keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import train_test_split

def prepare2train(train_set, validation_set, test_set, field_name):
    train_X = np.stack(train_set['file'])
    # train_y = to_categorical(train_bees[field_name].values)
    train_y = pd.get_dummies(train_set[field_name], drop_first=False)

    # Validation during training data to calc val_loss metric
    val_X = np.stack(validation_set['file'])
    val_y = pd.get_dummies(validation_set[field_name], drop_first=False)

    # Test data
    test_X = np.stack(test_set['file'])
    test_y = pd.get_dummies(test_set[field_name], drop_first=False)

    # Data augmentation - a little bit rotate, zoom and shift input images.
    generator = ImageDataGenerator(
            featurewise_center=False,  # set input mean to 0 over the dataset
            samplewise_center=False,  # set each sample mean to 0
            featurewise_std_normalization=False,  # divide inputs by std of the dataset
            samplewise_std_normalization=False,  # divide each input by its std
            zca_whitening=False,  # apply ZCA whitening
            # rotation_range=180,  # randomly rotate images in the range (degrees, 0 to 180)
            zoom_range=0.1,  # Randomly zoom image
            width_shift_range=0.2,  # randomly shift images horizontally (fraction of total width)
            height_shift_range=0.2,  # randomly shift images vertically (fraction of total height)
            horizontal_flip=True,  # randomly flip images
            vertical_flip=True
    )
    generator.fit(train_X)
    return generator, train_X, val_X, test_X, train_y, val_y, test_y


def split_balance(dataset, field_name):
    train_set, test_set = train_test_split(dataset, random_state=24)

    # Split train to train and validation datasets
    train_set, validation_set = train_test_split(train_set, test_size=0.1, random_state=24)

    # Balance
    ncat_bal = int(len(train_set) / train_set[field_name].cat.categories.size)
    train_set_bal = train_set.groupby(field_name, as_index=False).apply(
        lambda g: g.sample(ncat_bal, replace=True)).reset_index(drop=True)

    return train_set_bal, validation_set, test_set
