from keras.callbacks import ModelCheckpoint, EarlyStopping
from sklearn import metrics
import matplotlib.pyplot as plt
import numpy as np

import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D

from data_loader import load_data
from data_preparation import split_balance, prepare2train


# MY DATA
print('Loading data...')
data = load_data('../dataset')

print('Balancing data...')
(train_set, validation_set, test_set) = split_balance(data, 'label')

print('Data augmentation and splitting into training/test/validation sets..')
generator, train_X, val_X, test_X, train_y, val_y, test_y = prepare2train(train_set, validation_set, test_set, 'label')


# Statistics after finished learning
def eval_model(training, model, test_X, test_y, field_name):

    # Trained model analysis and evaluation
    f, ax = plt.subplots(2, 1, figsize=(5, 5))
    ax[0].plot(training.history['loss'], label="Loss")
    ax[0].plot(training.history['val_loss'], label="Validation loss")
    ax[0].set_title('%s: loss' % field_name)
    ax[0].set_xlabel('Epoch')
    ax[0].set_ylabel('Loss')
    ax[0].legend()

    # Accuracy
    ax[1].plot(training.history['acc'], label="Accuracy")
    ax[1].plot(training.history['val_acc'], label="Validation accuracy")
    ax[1].set_title('%s: accuracy' % field_name)
    ax[1].set_xlabel('Epoch')
    ax[1].set_ylabel('Accuracy')
    ax[1].legend()
    plt.tight_layout()
    plt.show()

    # Accuracy by classes
    test_pred = model.predict(test_X)

    acc_by_class = np.logical_and((test_pred > 0.5), test_y).sum() / test_y.sum()
    acc_by_class.plot(kind='bar', title='Accuracy by %s' % field_name)
    plt.ylabel('Accuracy')
    plt.show()

    # Print metrics
    print("Classification report")
    test_pred = np.argmax(test_pred, axis=1)
    test_truth = np.argmax(test_y.values, axis=1)
    print(metrics.classification_report(test_truth, test_pred, target_names=test_y.columns))

    # Loss function and accuracy
    test_res = model.evaluate(test_X, test_y.values, verbose=0)
    print('Loss function: %s, accuracy:' % test_res[0], test_res[1])


def create_model():
    model = Sequential()
    model.add(Conv2D(
        32, (3, 3), padding='same',
        # input_shape=train_X.shape[1:]
        input_shape=(32, 32, 3)
    ))
    model.add(Activation('relu'))
    model.add(Conv2D(32, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Conv2D(64, (3, 3), padding='same'))
    model.add(Activation('relu'))
    model.add(Conv2D(64, (3, 3)))
    model.add(Activation('relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(Activation('relu'))
    model.add(Dropout(0.5))
    model.add(Dense(train_y.columns.size))
    model.add(Activation('softmax'))

    opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
    model.compile(
        loss='categorical_crossentropy',
        optimizer=opt,
        metrics=['accuracy'])

    return model


def train():
    print('Building model...')
    model = create_model()

    earlystopper = EarlyStopping(monitor='loss', patience=10, verbose=1)
    # Save the best model during the traning
    checkpointer = ModelCheckpoint(
        'best_model1.h5',
        monitor='val_acc',
        verbose=1,
        save_best_only=True,
        save_weights_only=True)

    print('Trenowanie sieci rozpoczęte!')
    training = model.fit_generator(
        generator.flow(train_X, train_y, batch_size=60),
        epochs=40,
        validation_data=[val_X, val_y],
        steps_per_epoch=40,
        callbacks=[earlystopper, checkpointer]
    )

    # Get the best saved weights
    model.load_weights('best_model1.h5')

    # Call evaluation function
    eval_model(training, model, test_X, test_y, 'label')


train()
