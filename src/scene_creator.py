import cv2
import os
import random
import errno
import numpy as np
from PIL import Image
from imgaug import augmenters as iaa

DATASET_SIZE_PER_OBJECT = 500
RGBA = "RGBA"
RGB = "RGB"
imgH = 250
imgW = 250
scaleBg = iaa.Scale({"height": imgH, "width": imgW})
scaleBeforeSave = iaa.Scale({"height": 32, "width": 32})

# def create_mock():
#     dataset_path = '../dataset2'
#     name = 'white'
#     image = Image.open(f'{dataset_path}/{name}/{name}.jpg')
#     for i in range(50):
#         path = f"{dataset_path}/{name}/{name}_{str(i)}.jpg"
#         image.save(path)

# create_mock()


class SceneCreator:
    def __init__(self, backgrounds_path, objects_path, dataset_path):
        self.backgrounds_path = backgrounds_path
        self.objects_path = objects_path
        self.dataset_path = dataset_path

    def create_dataset_for_object(self, object_image_name):
        object_image_path = self.objects_path + "/" + object_image_name
        object_image = Image.open(object_image_path)

        for i in range(DATASET_SIZE_PER_OBJECT):
            # Load random image, convert to array, decrease size and convert back to Image type
            bg_image = Image.open(self.backgrounds_path + "/" + self.__get_random_bg_filename())
            bg_image = np.array(bg_image)
            bg_image = scaleBg.augment_image(bg_image)
            bg_image = Image.fromarray(bg_image)

            rotated = self.__random_rotation(object_image)
            translation = self.__random_translation(rotated.size, bg_image.size)

            target_location = self.__get_save_path(object_image_name, i)
            self.__create_folder(target_location)

            # paste our object (bullet) onto backround, at once with translation
            merged = self.__paste_object_to_bg(rotated, bg_image, translation)

            # decrease size of ready scene
            merged = np.array(merged)
            merged = scaleBeforeSave.augment_image(merged)
            merged = Image.fromarray(merged)

            merged.save(target_location)

    def __random_translation(self, object_size, bg_size):
        x_translation = random.randint(0, (bg_size[0] - object_size[0])//2) * self.__rand_sign()
        y_translation = random.randint(0, (bg_size[1] - object_size[1])//2) * self.__rand_sign()
        print(x_translation, y_translation)
        return x_translation, y_translation

    def __random_rotation(self, img):
        return img.rotate(self.__get_random_angle(), expand=1)

    def __load_random_background(self, filename):
        self.background = cv2.imread(self.input_dir + filename)

    def __get_random_bg_filename(self):
        bg_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), self.backgrounds_path))
        filename = random.choice([x for x in os.listdir(bg_dir) if os.path.isfile(os.path.join(bg_dir, x))])
        return filename

    def __get_save_path(self, filename, i):
        name, extension = filename.split('.')
        extension = 'jpg'
        return f"{self.dataset_path}/{name}/{name}_{str(i)}.{extension}"

    @staticmethod
    def __rand_sign():
        return 1 if random.random() < 0.5 else -1

    @staticmethod
    def __create_folder(location):
        if not os.path.exists(os.path.dirname(location)):
            try:
                os.makedirs(os.path.dirname(location))
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise

    @staticmethod
    def __paste_object_to_bg(object, bg, translation):
        bg_center = tuple(ti // 2 for ti in bg.size)
        object_offset = tuple(ti // 2 for ti in object.size)

        center_position = tuple(np.subtract(bg_center, object_offset))
        translated_position = tuple(np.add(center_position, translation))

        # to handle transparency, we need to convert image to RGBA
        merged = bg.convert(RGBA)  # sets alpha to 255
        merged.paste(object, translated_position, object)
        # we want to save as jpg (which doesnt hande transparency), so have to convert back to RGB
        merged = merged.convert(RGB)
        return merged

    @staticmethod
    def __get_random_angle():
        return random.randrange(10, 350, 10)

    @staticmethod
    def __pil_to_opencv(pil_image):
        return cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGBA2BGRA)


sceneC = SceneCreator("../dtd/merged_images", "../output", "../dataset4")

sceneC.create_dataset_for_object("hn_baracuda_power.png")
sceneC.create_dataset_for_object("lapua.png")
sceneC.create_dataset_for_object("bullet.png")
