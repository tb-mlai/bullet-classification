from image_loader import ImageLoader

INPUT_DIR = '../img/'
OUTPUT_DIR = '../output2/'
PIX_PER_MM = 2
OUTPUT_FORMAT = 'png'

# TODO:
# - create uniform method for extracting object from white bg


def main():
    image_loader = ImageLoader(INPUT_DIR, OUTPUT_DIR, OUTPUT_FORMAT, PIX_PER_MM, True)
    image_loader.load_image('lapua', 'jpg', 5)
    # image_loader.load_image('hn_baracuda_power', 'jpg', 14.9352)


if __name__ == "__main__":
    main()
