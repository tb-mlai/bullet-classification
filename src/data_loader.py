import os
import skimage.io
import pandas as pd
from sklearn.utils import shuffle

def read_img(file):
    img_channels = 3
    img = skimage.io.imread(file)
    return img[:, :, :img_channels]


def load_data(dataset_path):
    img_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), dataset_path))
    dirnames = [x for x in os.listdir(img_dir)]
    files = []
    labels = []

    for dirname in dirnames:
        dir = os.path.abspath(os.path.join(os.path.dirname(__file__), f"{dataset_path}/{dirname}"))
        for filename in os.listdir(dir):
            file = os.path.abspath(os.path.join(os.path.dirname(__file__), f"{dir}/{filename}"))
            files.append(read_img(file))
            labels.append(dirname)

    categories = pd.Categorical(labels)
    data_frame = pd.DataFrame(data={'file': files})
    data_frame['label'] = categories
    data_frame = shuffle(data_frame)

    return data_frame


if __name__ == "__data_loader__":
    data = load_data('../dataset')
    print('hello')
