import numpy as np
import cv2
from utils import show_img


class ImageLoader:
    def __init__(self, input_dir, output_dir, output_format, pix_per_mm, debug=False):
        self.input_dir = input_dir
        self.output_dir = output_dir
        self.output_format = output_format
        self.pix_per_mm = pix_per_mm
        self.debug = debug

    def load_image(self, filename, input_format, real_width):
        img = self.__load_image('.'.join([filename, input_format]))
        mask = self.__create_mask(img)
        cnt = self.__get_contour(mask)
        dimensions = self.__get_dimensions(cnt)
        img_transparent_bg = self.__transparentize_background(img, mask)
        img_cropped = self.__crop_image(img_transparent_bg, *dimensions)
        # img_resized = self.__resize_image(img_cropped, self.__get_resize_factor(dimensions[2], real_width))
        self.__save_image('.'.join([filename, self.output_format]), img_cropped)

    def __load_image(self, filename):
        original = cv2.imread(self.input_dir + filename)
        img = cv2.resize(original, None, fx=0.2, fy=0.2, interpolation=cv2.INTER_AREA)
        # img = original
        img = cv2.copyMakeBorder(img, 50, 50, 0, 0, cv2.BORDER_CONSTANT, value=[255, 255, 255])

        if self.debug:
            show_img(img)

        return img

    def __create_mask(self, img):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # get rid of noise
        gray_blurred = cv2.GaussianBlur(gray, (5, 5), 0)
        # gray_blurred = cv2.medianBlur(gray, 7)

        if self.debug:
            show_img(gray)
            show_img(gray_blurred, 'gray_blurred')

        # _, gray_blurred = cv2.threshold(gray_blurred, 200, 255, cv2.THRESH_TOZERO_INV)
        # _, gray_blurred = cv2.threshold(gray_blurred, 250, 255, cv2.THRESH_BINARY_INV)

        # TODO: find proper way of removing bg noise/shadow without damaging object
        # gray_blurred[gray_blurred > 190] = 255
        if self.debug:
            show_img(gray_blurred, 'gray_blurred after truncating')

        # create mask and add padding
        _, mask = cv2.threshold(gray_blurred, 250, 255, cv2.THRESH_BINARY_INV)

        # create uniform mask without holes
        kernel = np.ones((30, 30), np.uint8)
        mask_filled = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

        # mask_filled = mask.copy()
        # im2, contours, hierarchy = cv2.findContours(mask_filled, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        #
        # for cnt in contours:
        #     cv2.drawContours(mask_filled, [cnt], 0, 255, -1)

        if self.debug:
            show_img(mask, 'mask')
            show_img(mask_filled, 'mask-filled')

        return np.uint8(mask_filled)

    def __get_contour(self, img):
        edges = cv2.Canny(img, 30, 200)
        _, cnts, _ = cv2.findContours(edges.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnt = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

        if self.debug:
            show_img(edges, 'edges')

        return cnt

    def __save_image(self, filename, img):
        cv2.imwrite(self.output_dir + filename, img)

    @staticmethod
    def __transparentize_background(img, mask):
        b, g, r = cv2.split(img)
        rgba = [b, g, r, mask]
        return cv2.merge(rgba, 4)

    @staticmethod
    def __crop_image(img, x, y, w, h):
        return img[y:y + h, x:x + w].copy()

    @staticmethod
    def __resize_image(img, resize_factor):
        return cv2.resize(img, (0, 0), fx=resize_factor, fy=resize_factor)

    @staticmethod
    def __get_dimensions(cnt):
        x, y, w, h = cv2.boundingRect(cnt)
        return x, y, w, h
