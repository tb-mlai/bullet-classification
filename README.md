# Bullet classification

## About
This project was my first deep dive into ML/AI world. I wanted to get familiar with three main aspects:
1. OpenCV - basic operations, filters, image manipulation, line detection etc.
2. Keras - how to build and train neural nets
3. CNN - learning about the theory behind those specific nets increased a lot my understanding
 

A lot of data preparation, neural network building and finetuning was inspired by o taken from this great project:
https://www.kaggle.com/dmitrypukhov/honey-bee-health-detection-with-cnn

The final results of bullet detection are not satisfactory, as accuracy is below acceptable. This is a thing to be investigated further (if I wil have time and will to do so :))



## Installation and setup
1. Install python 3.6.7 architecture x64 - for 3.7 there was no tensorflow at that moment
2. I recommend PyCharm for IDE
3. After cloning project, run `pip install -r requirements.txt`
    to install `Shapely`, you need to download unofficial package - `https://www.lfd.uci.edu/~gohlke/pythonlibs/#shapely`, please download  `Shapely‑1.6.4.post1‑cp36‑cp36m‑win_amd64.whl` version
    then move the file to the project folder
4. Download backgroud images for fake learning data generation - `https://www.robots.ox.ac.uk/~vgg/data/dtd/`
5. All of the files from downloaded folder `dtd` should be moved to folder, so all of the images would end up in a flat list structure with no additional folders: `dtd/merged_images`
6. To generate 'scene' images (which will be used for neural network training), you need to run `scene_creator.py`, so in console: `python scene_creator.py`
7. To start training neural network, run file `cnn.py`